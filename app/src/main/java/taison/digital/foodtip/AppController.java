package taison.digital.foodtip;

import android.app.Activity;
import android.app.Application;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import taison.digital.foodtip.modules.ContextModule;
import taison.digital.foodtip.network.ApiService;
import timber.log.Timber;

/**
 * Created by Wanda on 6/7/2017.
 */

public class AppController extends Application {

    private AppControllerComponent appControllerComponent;

    public static AppController get(Activity activity) {
        return (AppController) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());
        appControllerComponent = DaggerAppControllerComponent.builder()
                .contextModule(new ContextModule(this)).build();

    }

    public AppControllerComponent getAppControllerComponent() {
        return appControllerComponent;
    }

}
