package taison.digital.foodtip.contracts;

import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;

/**
 * Created by Wanda on 6/9/2017.
 */

public interface LoginContract {

    void facebookLoginSuccess(CallbackManager callbackManager, LoginResult result);

    void facebookLoginFailed(CallbackManager callbackManager, FacebookException error);

    void facebookLoginCancelled(CallbackManager callbackManager, String cancellationMessage);

}
