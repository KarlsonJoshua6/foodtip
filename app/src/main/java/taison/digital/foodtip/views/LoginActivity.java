package taison.digital.foodtip.views;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import taison.digital.foodtip.AppController;
import taison.digital.foodtip.R;
import taison.digital.foodtip.components.DaggerLoginActivityComponent;
import taison.digital.foodtip.contracts.LoginContract;
import taison.digital.foodtip.network.ApiService;
import taison.digital.foodtip.presenters.LoginPresenter;
import timber.log.Timber;

/**
 * Created by Wanda on 6/7/2017.
 */

public class LoginActivity extends Activity implements LoginContract {

    @Inject
    ApiService apiService;

    @Inject
    LoginPresenter loginPresenter;

    @Inject
    CallbackManager mCallbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        DaggerLoginActivityComponent.builder()
                .appControllerComponent(AppController.get(this).getAppControllerComponent())
                .build().injectLoginActivity(LoginActivity.this);

        ButterKnife.bind(this);

    }

    @OnClick(R.id.flFacebookContainer)
    public void loginViaFacebook() {

        loginPresenter.processLogin(LoginActivity.this, this);

    }

    @Override
    public void facebookLoginSuccess(CallbackManager callbackManager, LoginResult result) {
        Toast.makeText(getApplicationContext(), getString(R.string.facebook_success) , Toast.LENGTH_SHORT).show();
    }

    @Override
    public void facebookLoginFailed(CallbackManager callbackManager, FacebookException error) {
        Toast.makeText(getApplicationContext(), getString(R.string.facebook_failed) + error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void facebookLoginCancelled(CallbackManager callbackManager, String cancellationMessage) {
        Toast.makeText(getApplicationContext(), cancellationMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
