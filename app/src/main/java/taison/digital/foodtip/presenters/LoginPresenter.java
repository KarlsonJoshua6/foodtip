package taison.digital.foodtip.presenters;

import android.os.Bundle;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.json.JSONObject;

import java.util.Arrays;

import taison.digital.foodtip.R;
import taison.digital.foodtip.contracts.LoginContract;
import taison.digital.foodtip.views.LoginActivity;
import timber.log.Timber;

/**
 * Created by Wanda on 6/9/2017.
 */

public class LoginPresenter {

    private CallbackManager mCallbackManager;

    public LoginPresenter(CallbackManager mCallbackManager) {
        this.mCallbackManager = mCallbackManager;
    }

    public void processLogin(final LoginActivity loginActivity, final LoginContract loginContract) {

        LoginManager.getInstance().logInWithReadPermissions(loginActivity, Arrays.asList("public_profile"));
        LoginManager.getInstance().registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        Timber.d("PROFILE: " + response.toString());

                    }
                });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "public_profile");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();


            }

            @Override
            public void onCancel() {

                loginContract.facebookLoginCancelled(mCallbackManager, loginActivity.getString(R.string.facebook_cancelled));

            }

            @Override
            public void onError(FacebookException error) {

                loginContract.facebookLoginFailed(mCallbackManager, error);

            }
        });

    }

}
