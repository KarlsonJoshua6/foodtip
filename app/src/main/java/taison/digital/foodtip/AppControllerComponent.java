package taison.digital.foodtip;

import com.squareup.picasso.Picasso;

import dagger.Component;
import taison.digital.foodtip.modules.ApiServiceModule;
import taison.digital.foodtip.modules.PicassoModule;
import taison.digital.foodtip.network.ApiService;
import taison.digital.foodtip.scopes.AppControllerScope;

/**
 * Created by Wanda on 6/7/2017.
 */

@AppControllerScope
@Component(modules = {ApiServiceModule.class, PicassoModule.class})
public interface AppControllerComponent {

    ApiService getApiService();

    Picasso getPicasso();

}
