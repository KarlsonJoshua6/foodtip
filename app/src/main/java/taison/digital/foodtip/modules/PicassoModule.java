package taison.digital.foodtip.modules;

import android.content.Context;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import taison.digital.foodtip.scopes.AppControllerScope;

/**
 * Created by Wanda on 6/7/2017.
 */

@Module(includes = {ContextModule.class, NetworkModule.class})
public class PicassoModule {

    @Provides
    @AppControllerScope
    public Picasso getPicasso(Context context, OkHttp3Downloader okHttp3Downloader) {

        return new Picasso.Builder(context)
                .downloader(okHttp3Downloader)
                .build();

    }

    @Provides
    @AppControllerScope
    public OkHttp3Downloader getOkHttp3Downloader(OkHttpClient okHttpClient) {
        return new OkHttp3Downloader(okHttpClient);
    }

}
