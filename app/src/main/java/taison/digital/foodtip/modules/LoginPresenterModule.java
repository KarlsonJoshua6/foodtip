package taison.digital.foodtip.modules;

import com.facebook.CallbackManager;

import dagger.Module;
import dagger.Provides;
import taison.digital.foodtip.presenters.LoginPresenter;
import taison.digital.foodtip.scopes.LoginActivityScope;

/**
 * Created by Wanda on 6/14/2017.
 */

@Module(includes = CallbackManagerModule.class)
public class LoginPresenterModule {

    @Provides
    @LoginActivityScope
    public LoginPresenter getLoginPresenter(CallbackManager callbackManager) {
        return new LoginPresenter(callbackManager);
    }

}
