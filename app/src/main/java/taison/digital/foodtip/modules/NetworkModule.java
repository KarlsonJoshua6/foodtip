package taison.digital.foodtip.modules;

import android.content.Context;

import java.io.File;
import java.io.IOException;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import taison.digital.foodtip.scopes.AppControllerScope;
import timber.log.Timber;

/**
 * Created by Wanda on 6/7/2017.
 */

@Module(includes = ContextModule.class)
public class NetworkModule {

    @Provides
    @AppControllerScope
    public File getCacheFile(Context context) {
        return new File(context.getCacheDir(), "okhttp_cache");
    }

    @Provides
    @AppControllerScope
    public Cache getCache(File cacheFile) {
        return new Cache(cacheFile, 10 * 1000 * 1000); //10 MB Cache
    }

    @Provides
    @AppControllerScope
    public HttpLoggingInterceptor getHttpLoggingInterceptor() {

        return new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                Timber.i(message);
            }
        }).setLevel(HttpLoggingInterceptor.Level.BASIC);

    }

    @Provides
    @AppControllerScope
    public Interceptor getInterceptor() {

        return new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Authorization", "Bearer ehkeyI97c8wRAWH1Essyrt4uYP2aakyHcr9x5qMfWmOcnCABP0cGkNUh9bwQ")
                        .build());
            }

        };

    }

    @Provides
    @AppControllerScope
    public OkHttpClient getOkHttpClient(HttpLoggingInterceptor httpLoggingInterceptor, Interceptor interceptor, Cache cache) {

        return new OkHttpClient.Builder()
                .addInterceptor(httpLoggingInterceptor)
                .addInterceptor(interceptor)
                .cache(cache)
                .build();

    }

}
