package taison.digital.foodtip.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import taison.digital.foodtip.network.ApiService;
import taison.digital.foodtip.scopes.AppControllerScope;

/**
 * Created by Wanda on 6/7/2017.
 */

@Module(includes = {NetworkModule.class, PicassoModule.class})
public class ApiServiceModule {

    @Provides
    @AppControllerScope
    public ApiService getApiService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @AppControllerScope
    public Retrofit getRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .baseUrl("http://upmood.taisondigital.com.ph")
                .build();
    }

}
