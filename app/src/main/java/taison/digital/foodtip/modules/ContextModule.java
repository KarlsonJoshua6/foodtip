package taison.digital.foodtip.modules;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import taison.digital.foodtip.scopes.AppControllerScope;

/**
 * Created by Wanda on 6/7/2017.
 */

@Module
public class ContextModule {

    private final Context context;

    public ContextModule(Context context) {
        this.context = context;
    }

    @Provides
    @AppControllerScope
    public Context getContext() {
        return context;
    }

}
