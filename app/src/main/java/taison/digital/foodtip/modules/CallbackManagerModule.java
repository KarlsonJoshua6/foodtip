package taison.digital.foodtip.modules;

import com.facebook.CallbackManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import taison.digital.foodtip.scopes.LoginActivityScope;

/**
 * Created by Wanda on 6/14/2017.
 */

@Module
public class CallbackManagerModule {

    @Provides
    @LoginActivityScope
    public CallbackManager getCallbackManager(){ return CallbackManager.Factory.create(); }

}
