package taison.digital.foodtip.components;

import dagger.Component;
import taison.digital.foodtip.AppControllerComponent;
import taison.digital.foodtip.modules.CallbackManagerModule;
import taison.digital.foodtip.modules.LoginPresenterModule;
import taison.digital.foodtip.scopes.LoginActivityScope;
import taison.digital.foodtip.views.LoginActivity;

/**
 * Created by Wanda on 6/14/2017.
 */

@LoginActivityScope
@Component(modules = {LoginPresenterModule.class, CallbackManagerModule.class}, dependencies = AppControllerComponent.class)
public interface LoginActivityComponent {

    void injectLoginActivity(LoginActivity loginActivity);

}
